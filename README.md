# gwdg_idm_api



## Getting started

Install the package via:
```
pip install gwdg_idm_api
```

## Usage

Checkout the example in `tests/examples.py`


## License

MIT
