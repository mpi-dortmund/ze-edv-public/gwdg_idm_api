from .benutzerverwaltung import (  # noqa
    BenutzerverwaltungAttributes,
    BenutzerverwaltungAttributes_MDMP,
)
from .dynamischeexchangeverteiler import DynamischeExchangeVerteilerAttributes  # noqa
from .exchangeverteiler import ExchangeVerteilerAttributes  # noqa
from .gruppenverwaltung import (  # noqa
    GruppenverwaltungAttributes,
    GruppenverwaltungAttributes_MDMP,
)
from .ldapverteiler import LDAPVerteilerAttributes  # noqa
from .mailinglisten import MailinglistenAttributes  # noqa
from .ressourcen import RessourcenAttributes  # noqa
from .selfservice import SelfServiceAttributes  # noqa
from .sharedmailbox import SharedMailboxAttributes  # noqa
